package com.example.android.effectivenavigation;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;



/**
 * Created by euiwonkim on 8/14/15.
 */
public class RandomYelpActivity extends Activity{

    private Button button;
    private ImageView img;
    private TextView text;
    private String imgurl;

    private String biz;
    private Uri uri;
    private Exception exception;

    private YelpAPI yelp;

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.yelp_demo);

        yelp = new YelpAPI();

        button = (Button) findViewById(R.id.yelp_button2);
        img = (ImageView) findViewById(R.id.imageView);

        imgurl = yelp.getImgUrl(yelp);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Picasso.with(RandomYelpActivity.this).load(imgurl).into(img);
            }
        });


    }
}
